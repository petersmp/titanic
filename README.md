Titanic Challenge
=================

My first Kaggle challenge.

Main page:
https://www.kaggle.com/c/titanic-gettingStarted


The guide that I am starting with to get a feel for doing this:
https://github.com/wehrley/wehrley.github.io/blob/master/SOUPTONUTS.md



Ideas
-----

- Can I pull last names to look for family links?
	- May be more or less likely to all perish?
